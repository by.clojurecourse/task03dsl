(defproject dsl "0.1.0-SNAPSHOT"
  :description "Task 03 - DSL for dates"
  :url "https://gitlab.com/sandboxapps/by.clojurecourse/task03dsl"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/core.match "0.3.0-alpha5"]]
  :main dsl.core)
