(ns dsl.core
  (:require [clojure.core.match :refer [match]]
            [clojure.test :refer :all])
  (:use clojure.walk))

(def cal (java.util.Calendar/getInstance))
(def today (java.util.Date.))
(def yesterday (do (.add cal java.util.Calendar/DATE -1) (.getTime cal)))
(def tomorrow (do (.add cal java.util.Calendar/DATE 2) (.getTime cal)))

(comment
  (defn one [] 1)

  ;; Примеры вызова
  (with-datetime
    (if (> today tomorrow) (println "Time goes wrong"))
    (if (<= yesterday today) (println "Correct"))
    (let [six (+ 1 2 3)
          d1 (today - 2 days)
          d2 (today + 1 week)
          d3 (today + six months)
          d4 (today + (one) year)]
      (if (and (< d1 d2)
               (< d2 d3)
               (< d3 d4))
        (println "DSL works correctly")))))


(defn d-gt [left right]
  (.after left right))


(defn d-lt [left right]
  (.before left right))


(defn d-lte [left right]
  (or (.before left right) (.equals left right)))


(defn d-gte [left right]
  (or (.after left right) (.equals left right)))


(defn date? [date]
  (instance? java.util.Date date))


(defn cmp-sym? [op]
  (some #(= op %) ['> '< '>= '<=]))


(defn add-sym? [op]
  (some #(= op %) ['+ '-]))


(defn get-date-cmp-sym [op]
  "Return comparing operation for date."
  (let [query [op]]
    (match query
      [(_ :guard #(= % '>))] d-gt
      [(_ :guard #(= % '<))] d-lt
      [(_ :guard #(= % '>=))] d-gte
      [(_ :guard #(= % '<=))] d-lte
      :else nil)))


(declare day days week weeks month months year years hour hours minute minutes)

(defn periods? [x]
  (some #(= x %) [day days week weeks month months year years hour hours
                  minute minutes]))

(defn get-date-period [x]
  (let [query [x]]
    (match query
      [(_ :guard #(some #{%} [day days week weeks]))] java.util.Calendar/DATE
      [(_ :guard #(some #{%} [month months]))] java.util.Calendar/MONTH
      [(_ :guard #(some #{%} [year years]))] java.util.Calendar/YEAR
      [(_ :guard #(some #{%} [hour hours]))] java.util.Calendar/HOUR
      [(_ :guard #(some #{%} [minute minutes]))] java.util.Calendar/MINUTE
      :else nil)))


(defn get-amount [x period]
  (if (some #(= % period) [week weeks])
      (* x 7)
      x))


(defn add-date [date op amount period]
  (do
    (.setTime cal date)
    (.add cal (get-date-period period) (op (get-amount amount period)))
    (.getTime cal)))


;; Режим Бога -- никаких подсказок.
;; Вы его сами выбрали ;-)
(defn convert-cmp [x]
  (if (sequential? x)
      (let [fst (first x)
            scnd (second x)
            thd (first (nnext x))
            last (second (nnext x))]
        (do
          (println `(identity ~scnd))
          (println (eval scnd))
          (println (type scnd))
          (if (and (cmp-sym? fst) (date? (eval scnd)) (date? (eval thd)))
              `(let [cmp-op# (~get-date-cmp-sym '~fst)]
                  (cmp-op# ~scnd ~thd))
              (if (and (add-sym? scnd) (int? (eval thd)) (periods? (eval last)))
                  `(~add-date ~fst ~scnd ~thd ~last)
                  x))))
      x))


(defmacro with-datetime [& code]
  (postwalk convert-cmp `(do ~@code)))


; TODO: remove
(defn one [] 1)

(with-datetime
  (if (> today tomorrow) (println "Time goes wrong"))
  (if (<= yesterday today) (println "Correct"))
  (let [six (+ 1 2 3)
        d1 (today - 2 days)
        d2 (today + 1 week)
        d3 (today + six months)
        d4 (today + (one) year)]
    (if (and (< d1 d2))
        :true
        :false)))


(macroexpand-1 `(with-datetime  (today - 2 days)))
(with-datetime  (today + 2 weeks))
(def six (+ 1 2 3))
(with-datetime  (today + six months))

(with-datetime
  (if (> today tomorrow) (println "Time goes wrong"))
  (if (<= yesterday today) (println "Correct"))
  (let [six (+ 1 2 3)]
    (if (and (< yesterday today))
      (println "DSL works correctly"))))

;; Примеры вызова
(with-datetime
  (if (> today tomorrow) (println "Time goes wrong"))
  (if (<= yesterday today) (println "Correct"))
  (let [six (+ 1 2 3)
        d1 (today - 2 days)
        d2 (today + 1 week)
        d3 (today + six months)
        d4 (today + (one) year)]
    (if (and (< d1 d2
              (< d2 d3)
              (< d3 d4)))
        (println "DSL works correctly"))))






; TODO: remove
(macroexpand-1 `(with-datetime (> today yesterday)))
(macroexpand-1 `(with-datetime (+ 1 2) (> today yesterday)))
(macroexpand-1 `(with-datetime (if (> today yesterday) :true :false)))
(with-datetime (if (> today yesterday) :true :false))
(with-datetime (> today yesterday) (> 2 3))
(with-datetime (> 2 3) (< today yesterday))
(with-datetime (> today yesterday))
(with-datetime (>= today today))
(with-datetime (if (> 3 2) :true :false))
(with-datetime (if (> today yesterday) :true :false))
(with-datetime (+ 1 2))
